Title: ::cogitri is now an official dev repo
Author: Rasmus Thomsen <cogitri@exherbo.org>
Content-Type: text/plain
Posted: 2018-02-23
Revision: 1
News-Item-Format: 1.0

The ::cogitri repository has been moved from Gitlab to git.exherbo.org
because it is an official dev repo now. The old repository on Gitlab won't be
updated anymore, please update your configs accordingly by changing the sync line
in /etc/paludis/repositories/cogitri.conf to the following:

    sync = git+https://git.exherbo.org/git/dev/cogitri.git
