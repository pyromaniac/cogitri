# Copyright 2012 NAKAMURA Yoshitaka
# Copyright 2013 Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require systemd-service udev-rules

if ever is_scm; then
    SCM_zfsimages_REPOSITORY="https://github.com/zfsonlinux/zfs-images.git"
    SCM_SECONDARY_REPOSITORIES="zfsimages"
    SCM_EXTERNAL_REFS="scripts/zfs-images:zfsimages"
fi

require github [ user=zfsonlinux release=${PNV} suffix=tar.gz ]

export_exlib_phases src_prepare src_install pkg_postinst

SUMMARY="Native ZFS for Linux"
DESCRIPTION="
The ZFS on Linux project provides a feature-complete implementation
of the ZFS file system (as defined by the OpenZFS project).

ZFS is a combined file system and logical volume manager.
The features of ZFS include protection against data corruption, support for high
storage capacities, efficient data compression, integration of the concepts of
filesystem and volume management, snapshots and copy-on-write clones, continuous
integrity checking and automatic repair, RAID-Z and native NFSv4 ACLs.
"
HOMEPAGE+=" https://zfsonlinux.org"

UPSTREAM_RELEASE_NOTES="https://github.com/zfsonlinux/zfs/releases/tag/zfs-${PV}"

LICENCES="CDDL-1.0"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# XXX: need root privileges. also, kernel modules should be build to run test
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.20]
        virtual/pkg-config
    build+run:
        dev-lang/python:*[>=3]
        dev-libs/libaio [[ note = [ Only used for libaiot test cases ] ]]
        net-libs/libtirpc
        sys-apps/util-linux
        sys-libs/zlib
        providers:eudev? ( sys-apps/eudev )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        providers:systemd? ( sys-apps/systemd )
        !sys-libs/spl [[
            description = [ zfs now includes spl ]
            resolution = uninstall-blocked-before
        ]]
    suggestion:
        sys-boot/dracut [[ description = [ For generating initramfs which include ZFS' modules ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    # Note that this only enables installation of systemd-units
    --enable-systemd
    --disable-asan
    --disable-pyzfs

    --with-config=user
    --with-dracutdir=/usr/$(exhost --target)/lib/dracut
    --with-mounthelperdir=/usr/$(exhost --target)/bin
    --with-python=3
    --with-systemdgeneratordir="${SYSTEMDSYSTEMUNITDIR}-generators"
    --with-systemdmodulesloaddir=/usr/$(exhost --target)/lib/modules-load.d
    --with-systemdpresetdir="${SYSTEMDSYSTEMUNITDIR}-preset"
    --with-systemdunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-tirpc
    --with-udevdir="${UDEVDIR}"
    --with-udevruledir="${UDEVRULESDIR}"
)

AT_M4DIR=( config )

zfs_src_prepare() {
    # Use prefixed cpp
    edo sed -i -e "s/cpp/$(exhost --tool-prefix)cpp/" config/kernel.m4

    autotools_src_prepare
}

zfs_src_install() {
    default

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/include/libzfs/linux

    dodir /usr/src/${PNV}
    edo mv * "${IMAGE}"/usr/src/${PNV}
    edo find "${IMAGE}" -type d -empty -delete
}

zfs_pkg_postinst() {
    elog "This package only builds ZFS' helper tools"
    elog "The kernel module source has been installed into /usr/src/${PNV}"
    elog "You have to build it yourself now and for every consequent kernel update:"
    elog "# cd /usr/src/${PNV}"
    elog "# ./configure --with-config=kernel --with-linux=<pathtoyourkernelsource>"
    elog "# make && make install"
}

