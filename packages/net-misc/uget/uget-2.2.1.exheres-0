# Copyright 2017-2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=urlget suffix=tar.gz ]
require gtk-icon-cache

SUMMARY="uGet, the Best Download Manager for Linux"
HOMEPAGE="http://ugetdm.com/"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    appindicator [[ description = [ Use AppIndicator for tray icon rather than a standard tray icon ] ]]
    gstreamer [[ description = [ Enable audio support via gstreamer ] ]]
    libnotify [[ description = [ Display notifications when downloads are added or finished ] ]]
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
        dev-libs/pcre
        net-misc/curl[>=7.19.1]
        x11-libs/gtk+:3[>=3.4]
        appindicator? ( dev-libs/libappindicator:0.1 )
        gstreamer? ( media-libs/gstreamer:= )
        libnotify? ( x11-libs/libnotify )
        providers:gnutls? ( dev-libs/libgcrypt )
        providers:libressl? ( dev-libs/libressl )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/14890943c52e0a5cd2a87d8a1c51cbffebee7cf9.patch
)

src_configure() {
    local myconf=(
        --enable-rss-notify
        # Unpackaged dependency
        --disable-pwmd
        $(option_enable appindicator)
        $(option_enable gstreamer)
        $(option_enable libnotify notify)
    )

    if option providers:libressl || option providers:openssl; then
        myconf+=(
            --with-openssl
            --without-gnutls
        )
    else
        myconf+=(
            --with-gnutls
            --without-openssl
        )
    fi

    econf "${myconf[@]}"
}

